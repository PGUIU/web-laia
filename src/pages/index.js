import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Intro</>,
    imageUrl: 'img/laiapic3.png',
    description: (
      <>
        With more people working remotely, living independently and going to therapy, we have created a voice assistant housed in a smart device
                that is dedicated to taking care of the user’s mental health.

      With a personalized approach, laia uses AI, NLP, voice recognition and facial recognition to monitor a user's 
      wellbeing and recommends actions based on their needs. 

      </>
    ),
  },
  {
    title: <>Solution</>,
    imageUrl: 'img/laiapic14.png',
    description: (
      <>
      LAIA is an AI voice assistant device that gives users accompaniment to traditional therapy. 
      LAIA connects users to their mental health providers.
      LAIA gives reminders of medications and consultations.
      LAIA tracks the user’s daily performance and stress levels so it can recommend actions to take care 
      of their mental health and provides a weekly report.
      </>
    ),
  },
  {
    title: <>How it works</>,
    imageUrl: 'img/laiapic15.png',
    description: (
      <>
        The user accesses the initial setup of the wizard through voice and camera interaction
        The voice assistant will recognize the user with the camera, through programs configured by experts,
         questions will be asked to the user to offer them in real time the state of stress, mood, etc
        The voice assistant has an internet connection via wifi and ethernet, you can receive updates and personalized programs.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`LAIA ${siteConfig.title}`}
      description="eHealth AI assistant <head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              href={('mailto:info@ainasolutions.com?Subject=Info LAIA')}Target={('_blank')}>
              Contact with us for more Info
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;

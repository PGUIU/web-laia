module.exports = {
  title: '',
  tagline: 'Language Artificial Intelligence Assistant',
  url: 'https://web-laia.vercel.app/',
  baseUrl: '/',
  favicon: 'img/fabicon.png',
  organizationName: '', // Usually your GitHub org/user name.
  projectName: '', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'LAIA',
      logo: {
        alt: 'My Site Logo',
        src: 'img/fabicon.png',
      },
      links: [
        {
          to: 'docs/doc1',
          activeBasePath: 'mission',
          label: 'Our Mission',
          position: 'left',
        },
        {to: 'vission', label: '', position: 'left'},
        {
          href: 'https://web-laia.vercel.app/',
          label: 'Mode',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Our',
          items: [
            {
              label: 'mission',
              to: 'docs/doc1',
            },
            {
              label: '',
              to: 'docs/doc3',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              //label: 'Linkedin',
              //href: 'https://www.linkedin.com/company/68914434/admin/',
            },
            {
              label: 'Youtube Channel',
              href: 'https://www.youtube.com/channel/UCoanNJ7xhBWm9cxHUNthZ9A',
            },
          ],
        },
        {
          title: '',
          items: [
            {
              label: '',
              to: '',
            },
            {
              label: 'LAIA',
              href: 'https://web-laia.vercel.app/',
            },
              {
              //label: 'Contact with us',
              //href: 'mailto:info@ainasolutions.com?Target=_blank?Subject=Info LAIA',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} LAIA from AINA.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};

